<?php

use Application\Page;

class PageTest extends PHPUnit_Framework_TestCase
{
	private $page;
	
	public function setUp(){
		$this->page = new Page;
	}
	
	public function testSetSlug()
	{
		$this->page->setSlug('page-slug');
		$this->assertEquals('page-slug', $this->page->getSlug(), "Slug wasn't defined correctly");
		
	}
	
	public function testeSetTitle(){
		$this->page->setTitle('Page Title');
		$this->assertEquals('Page Title', $this->page->getTitle(), "Title wasn't defined as we wanted.");
	}
	
	public function tearDown(){
	    parent::tearDown();
}
