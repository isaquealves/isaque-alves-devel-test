<?php
namespace Application;

/**
* This class defines the structure, elements and properties of a web page used by the application.
*
* @property integer  $id The numeric identifier of a page
* @property string   $keywords The keywords related with the content of the page
* @property string   $title The title of the webpage
* @property string   $slug The slug used to reference the page
* @property string   $description The description of the page
* @property string   $body The body/content of the page itself
* @property string   $author The author of the page content
* @property string   $insert_date The original insertion date of the page data
* @property string   $update_date The date in which the page had its data updated
*
* @author Isaque Alves <isaquealves@gmail.com>
* @access public 
*
* @filesource
*/

class Page{
	
	/**
	* @access protected
	*/
	protected $id;
	
	/**
	* @access protected
	*/
	protected $keywords;
	/**
	* @access protected
	*/
	protected $title;
	/**
	* @access protected
	*/
	protected $slug;
	/**
	* @access protected
	*/
	protected $description;
	/**
	* @access protected
	*/
	protected $body;
	/**
	* @access protected
	*/
	protected $author;
	/**
	* @access protected
	*/
	protected $insert_date;
	/**
	* @access protected
	*/
	protected $update_date;
	
	
	/* Setters */
	
	/**
	* Defines the value of property $id
	*/
	public function setId($id){
		$this->id = $id;
	}
	
	/**
	* Defines the value of property $title
	*/
	public function setTitle($title){
		$this->title = $title;
	}
	
	/**
	* Defines the value of property $slug
	*/
	public function setSlug($slug){
		$this->slug = $slug;
	}
	/**
	* Defines the value of property $keywords
	*/
	public function setKeywords($keywords){
		$this->keywords = $keywords;
	}
	
	/**
	* Defines the value of property $description
	*/
	public function setDescription($description){
		$this->description = $description;
	}
	
	/**
	* Defines the value of property $body
	*/
	public function setBody($body){
		$this->body = $body;
	}
	
	/**
	* Defines the value of property $author
	*/
	public function setAuthor($author){
		$this->author = $author;
	}
	
	/**
	* Defines the value of property $insert_date
	*/
	public function setInsertDate($idate){
		$this->insert_date = $idate;
	}
	
	/**
	* Defines the value of property $update_date
	*/
	public function setUpdateDate($udate){
		$this->update_date = $udate;
	}
	
	/* Getters */
	
	
	/**
	* Returns the value of property $id
	*/
	public function getId(){
		return $this->id;
	}
	
	/**
	* Returns the value of property $title
	*/
	public function getTitle(){
		return $this->title;
	}
	
	/**
	* Returns the value of property $title
	*/
	public function getSlug(){
		return $this->slug;
	}
	
	/**
	* Returns the value of property $description
	*/
	public function getKeywords(){
		return $this->keywords;
	}
	
	/**
	* Returns the value of property $description
	*/
	public function getDescription(){
		return $this->description;
	}
	
	/**
	* Returns the value of property $body
	*/
	public function getBody(){
		return $this->body;
	}
	
	/**
	* Returns the value of property $author
	*/
	public function getAuthor(){
		return $this->author;
	}
	
	/**
	* Returns the value of property $insert_date
	*/
	public function getInsertDate(){
		return $this->insert_date;
	}
	
	/**
	* Returns the value of property $update_date
	*/
	public function getUpdateDate(){
		return $this->update_date;
	}
	
}
