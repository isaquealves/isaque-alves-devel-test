<?php
namespace Application;



/**
* This class contains operations to be executed over database records, like, insertion, update and removal of data.
* @property $connection
*/
class PageDAO {
	
	private $connection = null;
	
	/**
	* The constructor of the class. It contains the definition of the $connection property
	*/
	public function __construct() {
	    
		$this->connection = new DB();
	}
	
	/**
	* The page insertion method. 
	* It makes use of the connection to create and execute statements over the database.
	* In case of exception, it will output the resultant error.
	*/
	public function insertPage($page){
		try {
			
			$stmt = $this->connection->prepare("INSERT INTO page (keywords, title, slug, description, body, author, insert_date) VALUES (?, ?, ?, ?, ?, ?, ?)");
			$stmt->bindValue(1, $page->getKeywords())
				 ->bindValue(2, $page->getTitle())
				 ->bindValue(3, $page->getSlug())
				 ->bindValue(4, $page->getDescription())
				 ->bindValue(5, $page->getBody())
				 ->bindValue(6, $page->getAuthor())
				 ->bindValue(7, $page->getInsertDate());
			$stmt->execute();
			$this->connection->close();
		} catch (PDOException $exception ){
			echo "An Error was detected: " . $exception->getMessage(); 
		}
	}
	
	/**
	* The page update method. 
	* @param $page Page A page instance
	* @param $field_array array An array containing the name of the modified fields.
	*/
	public function updatePage($page, $field_array){
		try{
			
			$field_string = $this->convert_array_assoc($field_array);
			$stmt = $this->connection->prepare("UPDATE page SET $field_string WHERE id=$page->getId()");
			$this->connection->beginTrasaction();
			
			for($i=0; $i == count($field_array); $i++)
			{
				$stmt->bindValue( $i + 1, $page->get . ucfirst($field_array[$i]) );
			}
			
			$stmt->execute();
			$this->connection->commit();
			$this->connection->close();
			
		}catch( PDOException $exception) {
			echo "There was an error: " . $exception->getMessage(); 
		}
	}
	
	/**
	* The page deletion method.
	* It will try to execute the deletion of the referenced page.
	* In case of successful deletion, will return the number of the affected database rows.
	* @param $pageId integer The page identifier.
	*/
	public function deletePage($pageId) {
		try {
			$del = $this->connection->exec("DELETE FROM page where id=$pageId");
			if( $del ){
				return $del;
			}
		}catch(PDOException $exception) {
			echo "There was an error: " . $exception->getMessage();
		}
	}
	
}
?>
