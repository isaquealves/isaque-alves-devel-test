<?php
namespace Application;

/**
* This class extends PDO and encapsulates operations of connection creation and closing
*
* @property $dsn string The datasource name
* @property $username string The username for the specified database (if required)
* @property $passwd string The password for accessing the database
* @property $handler PDOConnection A handler for the connection opened 
*/
class DB extends \PDO{
	
	private $dsn;
	private $username = "";
	private $passwd = "";
	public  $handler = null;

	function __construct($db="",$username="",$password="") {
		
		//$dsn = "mysql:host=localhost;dbname=$db";
		$this->dsn = "sqlite:mydb.db";
		$this->username = $username;
		$this->passwd = $password;
		
		try {
			if ( $this->handler == null ) {
				$db_handler = parent::__construct( $this->dsn , $this->username , $this->passwd );
				$this->handler = $db_handler;
				return $this->handler;
			}
		}catch ( PDOException $exception ) {
		echo 'Conexão falhou. Erro: ' . $exception->getMessage( );
		return false;
		}
	}
	
	
	function close( ) {
		$this->handler = NULL;
	}
	
}

?>
