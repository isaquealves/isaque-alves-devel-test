<?php

include_once("Autoloader.php");

use Application\DB;
use Application\Page;
use Application\PageDAO;


class Operations{
	
	private $page;
	
	public function createPage(){
		$this->page = new Application\Page();
		$this->page->setKeywords('main, principal, pagina inicial');
		$this->page->setSlug('pagina-principal');
		$this->page->setTitle('Página Principal');
		$this->page->setDescription('Página principal');
		$this->page->setAuthor('Isaque Alves');
		$this->page->setBody('<h1>Pagina Principal</h1>');
		$this->page->setInsertDate(date('M/d/Y'));
		$dao = new PageDAO();
		$dao->insertPage($this->page);
	}
	
}


$op = new Operations();
$op->createPage();
?>
