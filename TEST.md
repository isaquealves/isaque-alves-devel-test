# Instruções
O código do teste deverá ser armazenado dentro do diretório src deste repositório.

## Primeira etapa
Desenvolva um CRUD (CREATE, READ, UPDATE, DELETE), sem utilização de framework, orientado a objeto empregando uma arquitetura coerente.

Este CRUD deverá gerenciar Páginas Web, que são composta pelos seguintes campos:

*Os campos a seguir são obrigatórios, sinta-se a vontade para criar outros campos se achar necessário.*

+ id;

+ title;

+ slug;

+ description;

+ body;

+ author;

+ insert_date;

+ update_date;

## Segunda etapa
Crie uma documentação da API utilizando os comentários especiais destinados a documentação nas classes e métodos.
Para a criação desta documentação de API, sinta-se a vontade para utilizar o framework e/ou script de sua preferência, em qualquer linguagem.

## Terceira etapa
Opcionalmente, desenvolva os testes unitários com PHPUnit para o sistema que você desenvolveu.
