## Referências Técnicas

1. Quais foram os últimos dois livros técnicos que você leu?
> Engenharia de Software na Prática
> Padrões de Projeto - Soluções reutilizáveis de software orientado a objetos

2. Quais foram os últimos dois framework/CMS que você trabalhou?
> Zend Framework e Wordpress

3. Descreva os principais pontos positivos do seu framework favorito.
> Aplicações desenvolvidas de forma modular com Zend Framework possuem baixo acoplamento, e suportam injeção de dependência. Além disso, Aend Framework é extensivamente escalável devido ao fato de suportar o desenvolvimento modular, dotando com essa característica aplicações com ele desenvolvido.

4. Descreva os principais pontos negativos do seu framework favorito.
> Como  principal ponto negativo, digo isso em relação a ZF2, há pouca documentação técnica na forma de livros. 

5. O que é código de qualidade para você.
> Código de qualidade deve ser legível, bem documentado por meio de comentários em linguagem coerente, bem como fazer uso de mnemônicos sempre que possível.

## Conhecimento Linux

1. O que é software livre?
> Software Livre é ao mesmo tempo um Movimento sócio-político como um paradigma baseado no Movimento, o qual enfatiza que todo software deve respeitar o conjunto formado pelas 4 liberdades fundamentais de software: Liberdade de executar o software para qualquer propósito/finalidade; Liberdade de estudar o software e adaptá-lo conforme as próprias necessidades; Liberdade para redistribuir cópias e ajudar o próximo; Liberdade de aperfeiçoar o software e distribuir/liberar seus aperfeiçoamentos para benefício de toda a comunidade; O acesso ao código fonte é fundamental para a 2ª e 4 liberdades.

2. Qual o seu sistema operacional favorito?
> Ubuntu.

3. Já trabalhou com Linux ou outro Unix-like?
> Sim. entre os GNU/Linux, já trabalhei com Fedora e distribuições para servidores, como BrasilFW, SmoothWall... Também já tive contato com OpenSolaris (hoje Open Indiana) e alguns BSD.

4. O que é SSH?
> SSH é um ambiente seguro para execução de comandos, comumente usado para acessar remotamente servidores ou outros computadores clientes.

5. Quais as principais diferenças entre sistemas *nix e o Windows?
> Em matéria de diferença entre sistemas teríamos uma lista significativa, desde as relacionadas ao modo de funcionamento do kernel, até diferenças entre o funcionamento do escalonador de processos do kernel. Nos sistemas windows mais recentes, o kernel é modular, enquanto nos sistemas Unix, o modelo de kernel usado tem sido o kernel monolítico, embora hoje se admita que o kernel Linux seja 'semi-monolítico', suportando a compilação e carregamento de drivers/componentes de sistemas de forma semelhante aos kernels modulares.
> Quanto ao escalonamento de processos, o escalonador de processos utilizado em ambientes Windows é baseado em prioridades, o que pode eventualmente conduzir a travamentos dos sistemas quando um processo de usuário exige prioridade superior a um processo do sistema. Nos sistemas com kernel Linux, desde a versão 2.6, o escalonador de processos utiliza algoritmos como "Modular Scheduler", "Completely Fair Scheduler" e "CFS group scheduler", o que possibilita maior dinamicidade ao escalonador e previne 'race conditions' entre aplicações.
> Uma diferença sempre mencionada centra-se sobre o fato de sistemas windows serem desenvolvidos, distribuídos e comercializados sob o paradigma do Software Proprietário, enquanto os Sistemas Unix-like em sua maioria são gratuitos e em sua totalidade são Software Livre.

## Conhecimento de desenvolvimento

1. O que é GIT?
>  Git é um software utilizado para controle de versionamento distribuído. Facilita o controle do fluxo de desenvolvimento do trabalho de uma equipe evitando que elementos importantes sejam perdidos, descartados, sobrescritos acidentalmente, bem como possibilitando sempre o retorno do código a um estado anterior por meio de rollback.

2. Descreva um workflow simples de trabalho utilizando GIT.
    1. Administrador cria e publica um repositório de código;
    2. Colaboradores fazem 'forks' do repositório original;
    3. Colaboradores fazem um clone local do fork e realizam alterações;
    4. Colaboradores submetem alterações realizadas para o repositório local (commit);
    5. Colaboradores submetem alterações ao fork (pull);
    6. Colaboradores enviam solicitação de inserção de suas alterações no repositório público (pull request);
    7. Administrador realiza um push a partir do fork do desenvolvedor;

3. O que é PHP Data Objects?
> PDO é uma extensão da linguagem PHP que torna possível trabalhar de modo simples e transparente com operações com sistemas gerenciadores de bancos de dados;

4. O que é Database Abstract Layer?
> PHP DBAL ou DBA é uma extensão da linguagem PHP para trabalhar com sistemas de bancos de dados modernos que suportam armazenamento do tipo 'chave:valor'.

5. Você sabe o que é Object Relational Mapping? Se sim, explique.
> ORM, como é mais comumente referenciado, é resultado da aplicação de técnica de programação orientada a objetos. Com o uso de ORM, o gerenciamento dos dados se torna transparente para o desenvolvedor, na medida em que, cada tabela do banco de dados é tratada na aplicação como uma classe e cada item armazenado é identificado como uma instância da classe correspondente.

6. Como você avalia seu grau de conhecimento em Orientação a objeto?
> Bom. Já trabalhei com outras linguagens orientadas a objeto,como Java e Python.

7. O que é Dependency Injection?
> Injeção de dependência é uma técnica necessária à implementação de sistemas utilizando programação orientada a objetos, no sentido de reduzir o acomplamento e simplificar a distribuição da aplicação.

8. O significa a sigla S.O.L.I.D?
> É um acrônimo que faz referência aos 5 princípios fundamentais de orientação a objetos: Responsabilidade Única; Aberto/Fechado; Substituição de Liskov; Segregação de interface; Inversão de dependência.

9. Qual a finalidade do framework PHPUnit?
> A finalidade é possibilitar aos desenvolvedor um meio de garantirem que o produto de seu trabalho seja adequado aos requisitos, por meio da execução de testes simples da funcionalidades propostas.

10. Explique o que é MVC.
> MVC é uma das arquiteturas utilizadas para o desenvolvimento de aplicações para web. Seu diferencial encontra-se na possibilidade de separar a lógica da aplicação de sua camada de exibição. No centro da aplicação encontram-se os controllers, que realizam todas as operações lógicas do sistema. Os models constituem-se na parte da aplicação que contém as classes relacionadas às entidades do banco de dados.  Views são a camada externa, por assim dizer, ou seja, a camada com que o usuário tem maior contato, pois são responsáveis por receber os dados via controllers e repassá-los ao usuário como parte da interface gráfica.